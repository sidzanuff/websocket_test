package com.circumsolar.partytube;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.wicket.Application;
import org.apache.wicket.protocol.ws.WebSocketSettings;
import org.apache.wicket.protocol.ws.api.IWebSocketConnection;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.registry.IKey;
import org.apache.wicket.protocol.ws.api.registry.IWebSocketConnectionRegistry;
import org.apache.wicket.util.string.Strings;

import java.io.Serializable;

public class WebSocketConnectionDescriptor implements Serializable {

    private final String applicationName;
    private final String sessionId;
    private final IKey key;

    public WebSocketConnectionDescriptor(ConnectedMessage message) {
        this.applicationName = message.getApplication().getName();
        this.sessionId = message.getSessionId();
        this.key = message.getKey();
    }

    public IWebSocketConnection retrieveConnection() {
        Application application = Application.get(applicationName);
        WebSocketSettings webSocketSettings = WebSocketSettings.Holder.get(application);
        IWebSocketConnectionRegistry webSocketConnectionRegistry = webSocketSettings.getConnectionRegistry();
        return webSocketConnectionRegistry.getConnection(application, sessionId, key);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Strings.join(", ", applicationName, sessionId, key.toString());
    }
}

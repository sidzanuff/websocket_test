package com.circumsolar.partytube;

import java.util.HashMap;
import java.util.Map;

public class PlaylistManager {

    private static PlaylistManager instance = new PlaylistManager();

    public static PlaylistManager getInstance() {
        return instance;
    }

    private Map<String, Playlist> playlistMap = new HashMap<>();

    private PlaylistManager() {
    }

    public synchronized Playlist getPlaylist(String name) {
        if (!playlistMap.containsKey(name)) {
            Playlist playlist = new Playlist(name);
            playlistMap.put(name, playlist);
        }
        return playlistMap.get(name);
    }
}

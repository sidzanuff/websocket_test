package com.circumsolar.partytube;

import org.apache.log4j.Logger;
import org.apache.wicket.protocol.ws.api.IWebSocketConnection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WebSocketHelper {

    private static Logger logger = Logger.getLogger(WebSocketHelper.class);

    public static void sendMessage(Collection<WebSocketConnectionDescriptor> connectionDescriptors, String message) {
        List<WebSocketConnectionDescriptor> expiredDescriptors = new ArrayList<>();
        for (WebSocketConnectionDescriptor connectionDescriptor : connectionDescriptors) {
            IWebSocketConnection connection = connectionDescriptor.retrieveConnection();
            if (connection != null && connection.isOpen()) {
                try {
                    logger.debug("sending message " + message + " to " + connectionDescriptor);
                    connection.sendMessage(message);
                } catch (IOException e) {
                    logger.debug("removing expired descriptor " + connectionDescriptor);
                }
            } else {
                expiredDescriptors.add(connectionDescriptor);
            }
        }
        connectionDescriptors.removeAll(expiredDescriptors);
    }

}

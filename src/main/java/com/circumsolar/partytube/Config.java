package com.circumsolar.partytube;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {
    private static final Logger log = LoggerFactory.getLogger(Config.class);

    private static String os;
    private static boolean debug;

    public static boolean isDebug() {

        if (os == null) {
            os = System.getProperty("os.name");
            log.info("OS Environment Detected: " + os);
            debug = !os.toLowerCase().contains("linux");
        }

        return debug;
    }
}

package com.circumsolar.partytube.pages;

import com.circumsolar.partytube.Playlist;
import com.circumsolar.partytube.PlaylistManager;
import com.circumsolar.partytube.WebSocketConnectionDescriptor;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.WebSocketRequestHandler;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.message.TextMessage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.List;
import java.util.Objects;

public class PlaylistPage extends WebPage {
    private static final long serialVersionUID = 1L;

    private static PlaylistManager playlistManager = PlaylistManager.getInstance();
    private String playlistName;
    private WebSocketConnectionDescriptor connectionDescriptor;
    private WebMarkupContainer list;

    public PlaylistPage(final PageParameters parameters) {
        super();

        playlistName = parameters.get("name").toString();

        add(new WebSocketBehavior() {
            @Override
            protected void onConnect(ConnectedMessage message) {
                connectionDescriptor = new WebSocketConnectionDescriptor(message);
                getPlaylist().addListener(connectionDescriptor);
            }

            @Override
            protected void onMessage(WebSocketRequestHandler handler, TextMessage message) {
                if (Objects.equals(message.getText(), "KEEPALIVE")) {
                    return;
                }
                handler.add(list);
            }
        });

        add(new Label("playlistName", playlistName));

        list = new WebMarkupContainer("list");
        list.setOutputMarkupId(true);
        list.add(new ListView<String>("item", new AbstractReadOnlyModel<List<String>>() {
            @Override
            public List<String> getObject() {
                return getPlaylist().getItems();
            }
        }) {
            @Override
            protected void populateItem(ListItem<String> listItem) {
                listItem.add(new Label("name", listItem.getModelObject()));
            }
        });
        add(this.list);

        Form form = new Form("addForm");
        form.add(new AjaxButton("addButton", form) {
            @Override
            protected void onInitialize() {
                super.onInitialize();
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                super.onSubmit(target, form);
                getPlaylist().add();
                target.add(list);
            }
        });
        add(form);
    }

    private Playlist getPlaylist() {
        return playlistManager.getPlaylist(playlistName);
    }
}

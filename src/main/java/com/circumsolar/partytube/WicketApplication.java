package com.circumsolar.partytube;

import com.circumsolar.partytube.pages.PlaylistPage;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.ws.WebSocketSettings;
import org.apache.wicket.protocol.ws.api.registry.IWebSocketConnectionRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class WicketApplication extends WebApplication {

    protected static Logger logger = LoggerFactory.getLogger(WicketApplication.class);

    private ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    public WicketApplication() {
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            WebSocketSettings webSocketSettings = WebSocketSettings.Holder.get(WicketApplication.this);
            IWebSocketConnectionRegistry webSocketConnectionRegistry = webSocketSettings.getConnectionRegistry();
            webSocketConnectionRegistry.getConnections(WicketApplication.this).stream().forEach(connection -> {
                if (connection.isOpen()) {
                    try {
                        connection.sendMessage("KEEPALIVE");
                    } catch (IOException e) {
                        logger.warn("Could not send keepalive message", e);
                    }
                }
            });
        }, 1, 1, TimeUnit.MINUTES);
    }

    @Override
    public Class<? extends WebPage> getHomePage() {
        return PlaylistPage.class;
    }

    @Override
    public void init() {
        super.init();

        mountPage("/playlist", PlaylistPage.class);
    }

    @Override
    public RuntimeConfigurationType getConfigurationType() {
        if (Config.isDebug()) {
            return RuntimeConfigurationType.DEVELOPMENT;
        }
        return RuntimeConfigurationType.DEPLOYMENT;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scheduledExecutorService.shutdownNow();
    }
}

package com.circumsolar.partytube;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Playlist {

    private String name;
    private List<WebSocketConnectionDescriptor> listeners = new ArrayList<>();
    private List<String> items = new ArrayList<>();

    public Playlist(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addListener(WebSocketConnectionDescriptor connectionDescriptor) {
        listeners.add(connectionDescriptor);
    }

    public void removeListener(String id) {
        listeners.remove(id);
    }

    public List<String> getItems() {
        return items;
    }

    public synchronized void add() {
        items.add(new Date().toString());
        WebSocketHelper.sendMessage(listeners, "refresh");
    }
}
